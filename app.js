/**
 * CSV file must exists
 * parameters must be in particular order -> queryToGoogleMaps, language, coordinates, pathToCsv
 * Google APIKEY must be activated in https://console.developers.google.com (also every IP address must be activated)
 */

var json2csv = require('json2csv');
var fs = require('fs');
var request = require('request');
var async = require('async');
var appKeyId = "AIzaSyAENDlKw-mLkIXKYFSBiGLx-nz-YXL5C6Q";
var language = process.argv[3];
var location = process.argv[4];
var keyword = process.argv[2];
var lookingfor = {
    language: language,
    location: location,
    radius: "200000",
    keyword: keyword,
    key: appKeyId
};

var crawlerData = [];
var url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json";
var len;
var end = false;
var lenful = 0;
var timeout = 0;
var fulldata = [];
NearbySearch(lookingfor);
function NearbySearch(lookingfor){
    request({url: url, qs: lookingfor}, function (err, response, body) {
        if (err) return console.log(err);

        var data = JSON.parse(body);

        if (data['error_message']) return console.log(data['error_message']);

        len = data['results'].length;
        if(len === 20 && !end){
            var pageindex = data['next_page_token'];
            console.log('waiting on next page');
            var lookingfor = {
                pagetoken: pageindex,
                key: appKeyId,
                language: language,
                location: location,
                radius: "200000",
                keyword: keyword
            };
            getAllPlaceIds(data['results']);

            setTimeout(function(){ timeout+=100; NearbySearch(lookingfor); },timeout);///here is waiting against google ban
        }else{
            if(len != 0){
                getAllPlaceIds(data['results']);
            }
            console.log('done it',fulldata.length);
            lenful = fulldata.length;
            async.each(fulldata,function(k){
                console.log(k);
                setTimeout(function(){ timeout+=200; getPlaceDetails(k); },timeout);///here is waiting against google ban
            },function (err) {
                if (err) console.error(err.message);
            });
        }
    });
}
function getAllPlaceIds(data){
    async.forEachOf(data, function (value, key) {

        var result = data[key];
        var placeid = result['place_id'];
        if(placeid != undefined){
            if (fulldata.indexOf(placeid) == -1){
                fulldata.push(placeid);
            }else{
                end = true;
            }
        }
    }, function (err) {
        if (err) console.error(err.message);
    });
}
function getPlaceDetails(placeid)
{
    var url = "https://maps.googleapis.com/maps/api/place/details/json";
    var params = {
        placeid: placeid,
        key: appKeyId
    };
    getDetails(params,url)
}
function getDetails(params,url){
    request({url: url, qs: params}, function (err, response, body) {
        if (err) {
            lenful--;
            return console.log(err);
        }
        try {
            var data = JSON.parse(body);
            var result = data['result'];

            crawlerData.push({
                name: result['name'],
                address: result['formatted_address']
            });
            console.log(result['name']);

            if(lenful == crawlerData.length) {
                onFinished(crawlerData, ['name', 'address']);//more dynamic
            }
        } catch (e) {
            lenful--;
            if(lenful == crawlerData.length) {
                onFinished(crawlerData, ['name', 'address']);//more dynamic
            }
            console.log("not JSON");
        }
    });
};
function onFinished(data, fields)
{
    json2csv({data: data, fields: fields}, function (err, csv) {
        if (err) {
            console.log(err);
        }
        var path = process.argv[5];
        if (fs.existsSync(path)){
            fs.appendFile(path, csv, function (err) {
                if (err) {
                    throw err;
                }
                console.log('OK')
            });
        }else{
            fs.writeFile(path, csv, function (err) {
                if (err) {
                    throw err;
                }
                console.log('OK');
            });
        }

    });
}